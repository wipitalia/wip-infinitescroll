var wipwrp = window.wipwrp || {};

wipwrp.infiniteScroll = (function() {

	/* ----------------- private vars ------------------*/
	var self = {};
	var contents, pager, list, jsonkey, callback, $pager, $list, $currentPage, $nextPage, nextLink, pos, tempNode;
	var scrolledAll = false;
	var loadingContents = false;
	// by now it's assumed the active page is a li.active a

	/* ----------------- private methods ---------------*/

	self.init = function(opt) {
		list = opt.listSelector || '.listing';
		pager = opt.pagerSelector || '.pagination';
		jsonkey = opt.jsonResponseKey || '';
		callback = opt.callback;
		$list = document.querySelector(list);
		$pager = document.querySelector(pager);

		// if there is a pagination start listening
		if ($pager) {
			_scrollHandler();
			// style pagination as a loader
			$pager.addClass('loading');
		}
	};

	function _scrollHandler() {

		window.addEventListener('scroll', function() {
			// if you have more contents trig next page
			if ( !scrolledAll && !loadingContents && ($list.offsetHeight > 0) ) {
				_trigPaginator();
			}
		});
	}

	function _trigPaginator() {
		// find the link to use
		$currentPage = $pager.querySelector('li.active');
		$nextPage = $currentPage.nextElementSibling;

		if ($nextPage == null) {
			scrolledAll = true;
			$pager.remove();
			return false;
		};

		// check position and call 4 contents
		pos = $pager.offsetTop - windowScrollTop();

		if (pos < windowHeight()) {
			nextLink = $nextPage.querySelector('a:not([rel="last"])').getAttribute('href');
			loadingContents = true;
			getAjax({
				url: nextLink,
				success: function(res) {
					//if finish
					if ($nextPage.querySelector('a').getAttribute('rel') === 'next' || $nextPage === $pager.lastElementChild) {
						// disable trigging and remove pagination
						scrolledAll = true;
						$pager.remove();
					}else{
						if (jsonkey == '') {
							// create a temporary Node Element and append GET result
							tempNode = document.createElement('html');
							tempNode.innerHTML = res;
							contents = tempNode.querySelector(list).innerHTML;
						}else{
							contents = JSON.parse(res)[jsonkey];
						}
						// append contents
						$list.insertAdjacentHTML('beforeend',contents);
						//enable new loadings
						loadingContents = false;
						// set new active
						$currentPage.removeClass('active');
						$nextPage.addClass('active');
						//send callback
						if (callback != undefined) {
							callback();
						}
					}
				}
			});
}
}

return self;

})();