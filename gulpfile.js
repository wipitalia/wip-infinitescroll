var gulp = require('gulp');
var uglify = require('gulp-uglify');
var debug = require('gulp-debug');
var rename = require("gulp-rename");
var jsSources = 'src/*.js';
var jsDest = 'dist';
gulp.task('jscompression', function() {
    return gulp.src(jsSources)
        .pipe(debug({
            title: 'Js Src files: '
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest))
        .pipe(debug({
            title: 'Js Dest files: '
        }));
});
gulp.task('dev-jscompression', function() {
    return gulp.src(jsSources)
        .pipe(debug({
            title: 'Js Src files: '
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(jsDest))
        .pipe(debug({
            title: 'Js Dest files: '
        }));
});

gulp.task('minify', ['jscompression'], function() {
    gulp.watch('src/*.js', ['jscompression']);
});
gulp.task('dev-minify', ['dev-jscompression'], function() {
    gulp.watch('src/*.js', ['dev-jscompression']);
});
