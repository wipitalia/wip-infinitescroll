## wip infinite scroll

We assume the html of the pagination is structured as a ul that contains links.
We assume the li of the active pagine is defined by a "active" class.

#### Simple method:

Infinite scroll is desumed from a standard pagination:

	wipwrp.infiniteScroll.init({
		 'pagerSelector': '.pagination'
		,'listSelector': '.product-list'
		,'jsonkey': 'html'
	});

where:
 - pagerSelector: is the css selector of the html element that wraps the pagination (nav or directly the ul)
 - listSelector: is the css selector of the html element that wraps the paginated list
 - jsonkey: evauating this param you declare you're using json data. the value is a html string ready to use.


#### TODO: Advanced method:

We want to add a data attribute with a service, because now we are parsing client side the next page, with a service we can make it trough server.



### What it does (and how to style it)

It gives body a class "loading" and it appends in the pagionation wrapper (defined in 'pagerSelector' option).
Style this double class.
The scripts trigs the next page link, call asyncronously the page and parse it, it retrieves the contents and it appends it in the 'listSelector'.
